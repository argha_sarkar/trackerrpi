import time, gps, os, os.path, urllib, urllib2

def sendPostRequest(postUrl, keyValuePair):
    # A function which sends out HTTP Post requests
    data = urllib.urlencode(keyValuePair)
    request = urllib2.Request(postUrl, data)
    response = urllib2.urlopen(request)
    the_page = response.read()
    return 

# URL for php page on server which will add the data to the database
url = 'http://csumdf.uwcs.co.uk/gpsTracker/index.php'

#key value pair variable being initialised
values = {'name':'nothing yet'}

# Listen on port 2947 (gpsd) of localhost
session = gps.gps("localhost", "2947")
session.stream(gps.WATCH_ENABLE | gps.WATCH_NEWSTYLE)

count = 0

while True:
    # Gets the next reading from the GPS
    report = session.next()
    
    #print "While loop entered"
    
    if hasattr(report, 'lat') and hasattr(report, 'lon') and hasattr(report, 'time'):
    	#print str(count)
        count += 1
        csvLine = ("%s,%s\n" % (str(report.lon), str(report.lat)))
        #print csvLine[:-1]
        curTime = str(report.time)[11:-5]
        print curTime
        # Waits for 5 seconds to stop readings being sent too frequently 
        values = {'name' : csvLine, 'timestamp' : curTime}
        
        if count > 9:
        	print "Sending"
        	sendPostRequest(url, values)
        	print "Sent"
        	count = 0
def getMac():
	# Opens the mac address file and reads the mac address
	macFilePath = "/sys/class/net/eth0/address"
	macFile = open(macFilePath, 'r')

	textRead = macFile.read()
	macFile.close()
	
	count = 0
	macString = ""
	
	# This is used to remove the last line
	for line in textRead:
		if count < 17:
			macString += line
			count += 1
	
	return macString

def getSerial():
	# Opens the location of the file where the serial number is contained
	cpuInfoPath = "/proc/cpuinfo"
	cpuInfoFile = open(cpuInfoPath, 'r')
	
	textRead = cpuInfoFile.read()
	cpuInfoFile.close()
	
	count = 0
	serial = ""
	
	for line in textRead:
		count += 1
		#print "Line: " + str(count) + " - " + line
		if count < 278:
			if count > 261:
				serial += line
	
	return serial

macAddress = getMac()
serialNo = getSerial()

print "MAC address: --" + macAddress + "--"
print "Serial no: --" + serialNo + "--"
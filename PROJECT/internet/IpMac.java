/*
    GETS THE IP ADDRESS AND THE MAC ADDRESS OF THE SYSTEM
 */
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Stack;

/**
 * Created by Argha on 12/19/2014.
 */

public class IpMac {

    public String getIP() {
        InetAddress ip;
        try {
            ip = InetAddress.getLocalHost();
            return ip.getHostAddress();
        } catch (UnknownHostException e) {
            e.printStackTrace(System.err);
        }
        return null;
    }

     public String getMac() {
        InetAddress ip;
        try {
            ip = InetAddress.getLocalHost();
            NetworkInterface network = NetworkInterface.getByInetAddress(ip);
            
           
            
            byte[] mac = network.getHardwareAddress();

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < mac.length; i++) { 
                sb.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? ":" : ""));
            }

            return sb.toString();
        } catch (UnknownHostException e) { 
            return e.toString();
        } catch (SocketException e) {
            return e.toString();
        }
        
        //turn null;
    }

}


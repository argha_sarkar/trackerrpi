#!/bin/bash

# Command - ls -l /dev/ttyUSB* | grep -n 1970 | awk -F ":" '{print $1}'

USBNum=$(ls -l /dev/ttyUSB* | grep -n 1970 | awk -F ":" '{print $1}')

if [ ${USBNum:-null} = null ]
then
	echo "GPS not plugged in"	
else 
	(( USBNum-- ))
	echo "$USBNum."
fi
# Sockets
import socket

# Sleeping and waiting
import time
from threading import Thread

# Generating the random number that gets sent with each request
from random import randint

'''
	All the configuration information goes here.
'''

# SERVER ADDRESS ( IP ADDRESS / HOST )
serverAddr = "127.0.0.1"

# SERVER PORT NUMBER
portNo = 9999

def sendData(dataLine):
	
	try:
		# Creating a socket: mySoc
		#print "\nTrying to create a socket."
		mySoc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		#print "Socket successfully created.\n"
		
		# Connecting to the server socket
		mySoc.connect((serverAddr,portNo))
		#print "Successfully connected to the server.\n"
	
		#print "Attempting to send a message"
		mySoc.send(dataLine)
		print "Data sent successfully: " + dataLine + '\n'
		
	except socket.error as err:
		print "Socket creation failed with error: %s" %(err)
	
def startServer():
	# Keeps sending data to the Java server
	
	# Counter variable. 
	count = 0
	
	while (True):
		# Runs an infinite loop for sending data perpetually 
		try:
			if count > 99:
				sendData("STOP")
				time.sleep(2)
				break
			
			dataToSend = getRandomNo()
			sendData(dataToSend)
			count += 1
			time.sleep(2)
			
		except socket.error as err:
			print "Error in sending data: %s" %(err)
		

def getRandomNo():
	randNo = randint(10000, 99999)
	return "Random number: " + str(randNo)

def main():
	print "Running sendInfo()"
	startServer()

main()
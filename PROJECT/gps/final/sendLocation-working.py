import time, gps, os, os.path, urllib, urllib2

def sendPostRequest(postUrl, keyValuePair):
    # A function which sends out HTTP Post requests
    data = urllib.urlencode(keyValuePair)
    request = urllib2.Request(postUrl, data)
    response = urllib2.urlopen(request)
    the_page = response.read()
    return 

def getMac():
	# Opens the mac address file and reads the mac address
	macFilePath = "/sys/class/net/eth0/address"
	macFile = open(macFilePath, 'r')

	textRead = macFile.read()
	macFile.close()
	
	count = 0
	macString = ""
	
	# This is used to remove the last line
	for line in textRead:
		if count < 17:
			macString += line
			count += 1
	
	return macString

def getSerial():
	# Opens the location of the file where the serial number is contained
	cpuInfoPath = "/proc/cpuinfo"
	cpuInfoFile = open(cpuInfoPath, 'r')
	
	# Reads the content of the file and places it in a variable 'textRead'
	textRead = cpuInfoFile.read()
	cpuInfoFile.close()
	
	#Initialises serial number as an empty string
	count = 0
	serial = ""

	#Splits the text file info into lines
	linesTextRead = textRead.splitlines()
	
	#Line containing the serial number
	lineSerialNo = linesTextRead[-1:]
	
	serial = toStr(lineSerialNo)
	serial = serial[10:]
	
	return serial

def toStr(input):
	# Converts a list into a string
	output = ''.join(input)
	return output

# URL for php page on server which will add the data to the database
url = 'http://findmyride.co.uk/project/device/recLocation.php'

#key value pair variable being initialised
values = {'name':'nothing yet'}

# Listen on port 2947 (gpsd) of localhost -- Getting the GPS data
session = gps.gps("localhost", "2947")
session.stream(gps.WATCH_ENABLE | gps.WATCH_NEWSTYLE)

count = 0

# MAC address and serial number
macAddress = getMac()
serialNo = getSerial()
 
print "Mac address: " + macAddress
print "Serial no: " + toStr(serialNo)


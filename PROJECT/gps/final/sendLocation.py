import time, gps, os, os.path, urllib, urllib2
from urllib2 import URLError

def sendPostRequest(postUrl, keyValuePair):
    # A function which sends out HTTP Post requests
	try:
		data = urllib.urlencode(keyValuePair)
		request = urllib2.Request(postUrl, data)
		response = urllib2.urlopen(request)
		the_page = response.read()
		return True
	except URLError as e:
		print "Error connecting to the internet."
	except:
		print "Some other error with sending information to the server"

	# Something has failed so return false
	return False

def getMac():
	# Opens the mac address file and reads the mac address
	macFilePath = "/sys/class/net/eth0/address"
	macFile = open(macFilePath, 'r')

	textRead = macFile.read()
	macFile.close()
	
	count = 0
	macString = ""
	
	# This is used to remove the last line
	for line in textRead:
		if count < 17:
			macString += line
			count += 1
	
	return macString

def getSerial():
	# Opens the location of the file where the serial number is contained
	cpuInfoPath = "/proc/cpuinfo"
	cpuInfoFile = open(cpuInfoPath, 'r')
	
	# Reads the content of the file and places it in a variable 'textRead'
	textRead = cpuInfoFile.read()
	cpuInfoFile.close()
	
	#Initialises serial number as an empty string
	count = 0
	serial = ""

	#Splits the text file info into lines
	linesTextRead = textRead.splitlines()
	
	#Line containing the serial number
	lineSerialNo = linesTextRead[-1:]
	
	serial = toStr(lineSerialNo)
	serial = serial[10:]
	
	return serial
	
def toStr(input):
	# Converts a list into a string
	output = ''.join(input)
	return output

# URL for php page on server which will add the data to the database
url = 'https://findmyride.co.uk/project/device/recLocation.php'

#key value pair variable being initialised
values = {'name':'nothing yet'}

# Listen on port 2947 (gpsd) of localhost
session = gps.gps("localhost", "2947")
session.stream(gps.WATCH_ENABLE | gps.WATCH_NEWSTYLE)

count = 0

# MAC address and serial number
macAddress = getMac()
serialNo = getSerial()

print "Mac address: " + macAddress
print "Serial no: " + serialNo

while True:
	# Gets the next reading from the GPS
	report = session.next()

	#print "While loop entered"

	if hasattr(report, 'lat') and hasattr(report, 'lon') and hasattr(report, 'time') and hasattr(report, 'speed'):
		#print str(count)
		count += 1

		# The elements which
		curTime = str(report.time)
		curSpeed = str(report.speed)
		curLat = str(report.lat)
		curLong = str(report.lon)
		#print curTime
		# Waits for 5 seconds to stop readings being sent too frequently
		values = {'time_device' : curTime, 'speed' : curSpeed, 'lat' : curLat, 'long' : curLong, 'serial' : serialNo, 'mac' : macAddress}
		print values
		
		if count > 9:

			print "Sending"
			print "Time: " + curTime + "- Speed: " + curSpeed + "- Lat: " + curLat + "- Long: " + curLong

			sentStatus = sendPostRequest(url, values)
			if sentStatus == True:
				print "Sent"
				count = 0
        	

import time, gps, os, os.path, urllib, urllib2

# Listen on port 2947 (gpsd) of localhost
session = gps.gps("localhost", "2947")
session.stream(gps.WATCH_ENABLE | gps.WATCH_NEWSTYLE)

while True:
	
	report = session.next()
	
	if hasattr(report, 'speed'):
		print str(report.speed)
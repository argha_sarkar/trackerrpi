import time, os, os.path, urllib, urllib2, gps
# Sockets and threading
import socket
from threading import Thread

def sendPostRequest(postUrl, keyValuePair):
        # A function which sends out HTTP Post requests
        data = urllib.urlencode(keyValuePair)
        request = urllib2.Request(postUrl, data)
        response = urllib2.urlopen(request)
        the_page = response.read()
        return 

def getMac():
        # Opens the mac address file and reads the mac address
        macFilePath = "/sys/class/net/eth0/address"
        macFile = open(macFilePath, 'r')

        textRead = macFile.read()
        macFile.close()

        count = 0
        macString = ""

        # This is used to remove the last line
        for line in textRead:
                if count < 17:
                        macString += line
                        count += 1

        return macString

def getSerial():
        # Opens the location of the file where the serial number is contained
        cpuInfoPath = "/proc/cpuinfo"
        cpuInfoFile = open(cpuInfoPath, 'r')

        # Reads the content of the file and places it in a variable 'textRead'
        textRead = cpuInfoFile.read()
        cpuInfoFile.close()

        #Initialises serial number as an empty string
        count = 0
        serial = ""

        #Splits the text file info into lines
        linesTextRead = textRead.splitlines()

        #Line containing the serial number
        lineSerialNo = linesTextRead[-1:]

        serial = toStr(lineSerialNo)
        serial = serial[10:]

        return serial

def toStr(input):
        # Converts a list into a string
        output = ''.join(input)
        return output

def sendDataOverSocket(dataLine, serverAdd, port):
        # Sends string data over a socket. Aimed at sending data to a Java daemon running on the localhost	
        try:
                # Creating a socket: mySoc
                #print "\nTrying to create a socket."
                mySoc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                #print "Socket successfully created.\n"

                # Connecting to the server socket
                mySoc.connect((serverAdd,port))
                #print "Successfully connected to the server.\n"

                #print "Attempting to send a message"
                mySoc.send(dataLine)
                print "Data sent successfully: " + dataLine + '\n'

        except socket.error as err:
                print "Socket creation failed with error: %s" %(err)


#-----------------------------------------------------------------------------------------

# Hostname of local java daemon
javaHost = "127.0.0.1"
# Port number of java daemon
javaPortNo = 9999

# URL for php page on server which will add the data to the database
url = 'http://findmyride.co.uk/project/device/recLocation.php'

#key value pair variable being initialised
values = {'name':'nothing yet'}
'''
        Key-value pair in a String form. Its in a string form so that it can be sent over easily over the socket.
        Key and the value are separted using a colon, " : ".
        Different key-value pairs are separtated using a comma. " , "
'''
valuesStr = "name:nothing yet"

# Listen on port 2947 (gpsd) of localhost
session = gps.gps("localhost", "2947")
session.stream(gps.WATCH_ENABLE | gps.WATCH_NEWSTYLE)

count = 0

# MAC address and serial number
macAddress = getMac()
serialNo = getSerial()

print "Mac address: " + macAddress
print "Serial no: " + serialNo

while True:
        # Gets the next reading from the GPS
        report = session.next()

        #print "While loop entered"

        if hasattr(report, 'lat') and hasattr(report, 'lon') and hasattr(report, 'time') and hasattr(report, 'speed'):
                #print str(count)
                count += 1

                # The elements which 
                curTime = str(report.time)
                curSpeed = str(report.speed)
                curLat = str(report.lat)
                curLong = str(report.lon)
                #print curTime

                # Generates key value array to be sent for the direct HTTP POST Request
                values = {'time_device' : curTime, 'speed' : curSpeed, 'lat' : curLat, 'long' : curLong, 'serial' : serialNo, 'mac' : macAddress}

                # Generates the key value array in a string format to send over socket to the Java daemon
                valuesStr = "time_device:" + curTime + ",speed:" + curSpeed + ",lat:" + curlat + ",long:" + curLong + ",serial:" + serialNo + ",mac:" + macAddress

                if count > 9:
                        print "Sending"
                        print "Time: " + curTime + "- Speed: " + curSpeed + "- Lat: " + curLat + "- Long: " + curLong
                        #sendPostRequest(url, values)

                        #sendDataOverSocket(valuesStr, javaHost, javaPortNo)

                        print "Sent"
                        count = 0